#include <Servo.h>
Servo m1, m2, m3, m4;
double channel;

void setup() {
  // put your setup code here, to run once:
  m1.attach(2);
  m2.attach(3);
  m3.attach(4);
  m4.attach(5);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(8, INPUT);
  
  Serial.begin(9600);

  Serial.print("setup\n");
  Serial.print("Connect battery...\n\n");

  m1.writeMicroseconds(2000); 
  m2.writeMicroseconds(2000); 
  m3.writeMicroseconds(2000); 
  m4.writeMicroseconds(2000); 
  delay(4000);
  
  m1.writeMicroseconds(1000); 
  m2.writeMicroseconds(2000); 
  m3.writeMicroseconds(1000); 
  m4.writeMicroseconds(1000); 
  
  
  Serial.print("ESC calibrated\n");
  delay(1000);

}

void loop() {
   channel = pulseIn(8, HIGH);

   m1.writeMicroseconds(channel);
   m2.writeMicroseconds(channel);
   m3.writeMicroseconds(channel);
   m4.writeMicroseconds(channel);
   Serial.println(channel);
}


