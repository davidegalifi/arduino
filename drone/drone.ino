#include <Servo.h>
#include<Wire.h>
const bool DEBUG_ENABLED = false;

Servo m1, m2, m3, m4;

short int buzzer = 12;
short int battery = 7;

byte last_channel_1;
int throttle;
unsigned long timer_1, current_time;

const int MPU_addr = 0x68; // I2C address of the MPU-6050
int16_t Tmp, gyro_x, gyro_y, gyro_z;
float acc_pitch_cal, acc_roll_cal, pitch_cal, roll_cal;
long loop_timer;
float angle_pitch, angle_roll;
float acc_x, acc_y, acc_z, acc_total_vector;
float angle_roll_acc, angle_pitch_acc;
float angle_pitch_output, angle_roll_output;
boolean set_gyro_angles;

// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void setup() {
  m1.attach(3);
  m2.attach(4);
  m3.attach(5);
  m4.attach(6);
    
  pinMode(buzzer, OUTPUT);  // Buzzer pin
  pinMode(8, INPUT);        // Receiver pin
  pinMode(battery, OUTPUT);  // battery

  if(DEBUG_ENABLED)  Serial.begin(9600);

  // Enable interrupt on throttle input
  PCICR |= (1 << PCIE0);                                                    //Set PCIE0 to enable PCMSK0 scan.
  PCMSK0 |= (1 << PCINT0);                                                  //Set PCINT0 (digital input 8) to trigger an interrupt on state change.

  // -------- Setup MPU ----------
  println ("Setup MPU");
  buzzOne();
  set_gyro_angles = false;
  setup_mpu_6050 ();
  
  println ("start calibration");
  delay (250);  
  calibrate_mpu ();  
  println ("Setup complete");  
  loop_timer = micros ();
  buzzTwo();
  // ------- end setup MPU --------

  // ----- Initialise ESCs ----------
  digitalWrite(battery, LOW);
  print("Connect battery...\n\n");
  
  m1.writeMicroseconds(2000); 
  m2.writeMicroseconds(2000); 
  m3.writeMicroseconds(2000); 
  m4.writeMicroseconds(2000); 
  delay(1000);
  digitalWrite(battery, HIGH);
   
  delay(500);

  m1.writeMicroseconds(1000); 
  m2.writeMicroseconds(1000); 
  m3.writeMicroseconds(1000); 
  m4.writeMicroseconds(1000); 

  print("ESC calibrated\n");
  // ----- end initialise ESCs ----------
  
  delay(500);
}

// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================
void loop() {  
  // throttle is read through the interrupting routine.
  m1.writeMicroseconds(throttle);
  m2.writeMicroseconds(throttle);
  m3.writeMicroseconds(throttle);
  m4.writeMicroseconds(throttle);

  read_mpu_6050_data ();

  // ACCELEROMETER angles data
  accelerometer_data ();

  angle_pitch_acc -= acc_pitch_cal;
  angle_roll_acc -= acc_roll_cal;

  adjust_gyro_data ();
  angle_pitch_output = angle_pitch - pitch_cal;
  angle_roll_output = angle_roll - roll_cal;
  print_data ();
  while (micros () - loop_timer < 4000); //Wait until the loop_timer reaches 4000us (250Hz) before starting the next loop
  loop_timer = micros (); //Reset the loop timer
}

// -------------------------------------------------
// ----- INTERRUPT ROUTINE for throttle ------------
// -------------------------------------------------
ISR(PCINT0_vect){
  current_time = micros();
  //Channel 1=========================================
  if((PINB & B00000001)&& last_channel_1 == 0){                  //Input 8 changed from 0 to 1.
      last_channel_1 = 1;                                       //Remember current input state.
      timer_1 = current_time;                                   //Set timer_1 to current_time
  }
  else if(last_channel_1 == 1){                                 //Input 8 is not high and changed from 1 to 0.
    last_channel_1 = 0;                                         //Remember current input state.
    throttle = current_time - timer_1;                    //Channel 1 is current_time - timer_1.
  }
}

void buzzOne(){
  tone(buzzer, 1000);
  delay(125);
  noTone(buzzer);
  delay(125);
}

void buzzTwo(){
  tone(buzzer, 1000);
  delay(125);
  noTone(buzzer);
  delay(125);
  tone(buzzer, 1000);
  delay(125);
  noTone(buzzer);
  delay(125);
}

void buzzerError(){
  tone(buzzer, 1000);
  delay(250);
  noTone(buzzer);
  delay(250);
  tone(buzzer, 1000);
  delay(250);
  noTone(buzzer);
  delay(250);
}



void print_data () {
    if (DEBUG_ENABLED == true) {
        //print(" | Tmp = "); print(Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet

        print (angle_pitch_output);
        print ("\t");
        println (angle_roll_output);
    }
}
void read_mpu_6050_data () {
    Wire.beginTransmission (MPU_addr);
    Wire.write (0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
    Wire.endTransmission (false);
    Wire.requestFrom (MPU_addr, 14, true); // request a total of 14 registers
    acc_x = Wire.read () << 8 | Wire.read (); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
    acc_y = Wire.read () << 8 | Wire.read (); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    acc_z = Wire.read () << 8 | Wire.read (); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    Tmp = Wire.read () << 8 | Wire.read (); // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    gyro_x = Wire.read () << 8 | Wire.read (); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    gyro_y = Wire.read () << 8 | Wire.read (); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    gyro_z = Wire.read () << 8 | Wire.read (); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
}

void adjust_gyro_data () {
    //  GYRO angle calculations
    //0.0000611 = 1 / (250Hz / 65.5) dt integration
    angle_pitch += gyro_x * 0.0000611; //Calculate the traveled pitch angle and add this to the angle_pitch variable
    angle_roll += gyro_y * 0.0000611; //Calculate the traveled roll angle and add this to the angle_roll variable

    //0.000001066 = 0.0000611 * (3.142(PI) / 180degr) The Arduino sin function is in radians
    angle_pitch += angle_roll * sin (gyro_z * 0.000001066); //If the IMU has yawed transfer the roll angle to the pitch angel
    angle_roll -= angle_pitch * sin (gyro_z * 0.000001066); //If the IMU has yawed transfer the pitch angle to the roll angel

    angle_pitch = angle_pitch * 0.98 + angle_pitch_acc * 0.02; //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
    angle_roll = angle_roll * 0.98 + angle_roll_acc * 0.02; //Correct the drift of the gyro roll angle with the accelerometer roll angle
}

void accelerometer_data () {
    //Accelerometer angle calculations
    acc_total_vector = sqrt ((acc_x * acc_x) + (acc_y * acc_y) + (acc_z * acc_z)); //Calculate the total accelerometer vector

    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_acc = asin ((float) acc_y / acc_total_vector) * 57.296; //Calculate the pitch angle
    angle_roll_acc = asin ((float) acc_x / acc_total_vector) * -57.296; //Calculate the roll angle

    //Place the MPU-6050 spirit level and note the values in the following two lines for calibration
    angle_pitch_acc -= 0.0; //Accelerometer calibration value for pitch
    angle_roll_acc -= 0.0; //Accelerometer calibration value for roll
}

void calibrate_mpu () {
    float pitch_sum, roll_sum;

    // Avg Accelerometer
    for (int cal_int = 0; cal_int < 500; cal_int++) {
        //if(cal_int % 125 == 0)print(".");                              //Print a dot on the LCD every 125 readings

        read_mpu_6050_data (); //Read the raw acc and gyro data from the MPU-6050
        accelerometer_data ();
        pitch_sum += angle_pitch_acc;
        roll_sum += angle_roll_acc;

        delay (3); //Delay 3us to simulate the 250Hz program loop
    }

    acc_pitch_cal = pitch_sum / 500; //Divide the gyro_x_cal variable by 500 to get the avarage offset
    acc_roll_cal = roll_sum / 500; //Divide the gyro_y_cal variable by 500 to get the avarage offset

    println ("Acc Calibration");
    print ("pitch: ");
    println (acc_pitch_cal);
    print ("roll: ");
    println (acc_roll_cal);

    // Gyro Calibration
    pitch_sum = 0;
    roll_sum = 0;
    loop_timer = micros ();
    angle_pitch = acc_pitch_cal; //angle_pitch; //Set the gyro pitch angle equal to the accelerometer pitch angle 
    angle_roll = acc_roll_cal; //angle_roll; //Set the gyro roll angle equal to the accelerometer roll angle 

    read_mpu_6050_data ();
    accelerometer_data ();

    angle_pitch_acc -= acc_pitch_cal;
    angle_roll_acc -= acc_roll_cal;

    angle_pitch = angle_pitch_acc; //Set the gyro pitch angle equal to the accelerometer pitch angle 
    angle_roll = angle_roll_acc; //Set the gyro roll angle equal to the accelerometer roll angle 

    for (int ca = 0; ca < 1000; ca++) {
        read_mpu_6050_data ();

        // ACCELEROMETER angles data
        accelerometer_data ();

        angle_pitch_acc -= acc_pitch_cal;
        angle_roll_acc -= acc_roll_cal;

        adjust_gyro_data ();

        if (ca > 500) {
            pitch_sum += angle_pitch;
            roll_sum += angle_roll;
        }

        while (micros () - loop_timer < 4000); //Wait until the loop_timer reaches 4000us (250Hz) before starting the next loop
        loop_timer = micros (); //Reset the loop timer
    }

    pitch_cal = pitch_sum / 500;
    roll_cal = roll_sum / 500;

    println ("Post Calibration");
    print ("pitch: ");
    println (pitch_cal);
    print ("roll: ");
    println (roll_cal);

    delay (1000);
}

void setup_mpu_6050 () {
    Wire.begin ();
    Wire.beginTransmission (MPU_addr);
    Wire.write (0x6B); // PWR_MGMT_1 register
    Wire.write (0); // set to zero (wakes up the MPU-6050)
    Wire.endTransmission (true);
}



void print(String input){
  if(DEBUG_ENABLED){
    Serial.print(input);
  }
}
void println(String input){
   if(DEBUG_ENABLED){
    Serial.println(input);
  }
}
void print(float input){
  if(DEBUG_ENABLED){
    Serial.print(input);
  }
}
void println(float input){
   if(DEBUG_ENABLED){
    Serial.println(input);
  }
}

